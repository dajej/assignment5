<?php

class Club{
	private $id = null;
	private $name = null;
	private $city = null;
	private $county = null;

	public function __construct($doc, $dbnode)  
    {
		if($dbnode){
        $this->id = $doc->getAttribute("id");
        $this->name = $doc->getElementsByTagName("Name")->item(0)->nodeValue;
	    $this->city = $doc->getElementsByTagName("City")->item(0)->nodeValue;
	    $this->county = $doc->getElementsByTagName("County")->item(0)->nodeValue;
    
		$stmt = $dbnode->prepare("INSERT INTO clubs(id, name, city, county) 
									VALUES(:id, :name, :city, :county)");
		$stmt->bindValue(':id', $this->id);
		$stmt->bindValue(':name', $this->name);
		$stmt->bindValue(':city', $this->city);
		$stmt->bindValue(':county', $this->county);
		$stmt->execute();
}
else{
	echo "dbnode failed";
		}
	}
}
?>