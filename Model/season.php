<?php

class Season {
	public $fallYear = null;
	public $userNname = null;
	public $id = null;
	public $totalDistance = 0;

	public function __construct($distance, $year, $club, $name, $dbnode)  
    {
		if($dbnode){
        	$this->fallYear = $year;
	    	$this->id = $club;
	    	$this->userName = $name;
	    	$this->totalDistance = $distance;
	    	if ($club != null)
			$stmt = $dbnode->prepare("INSERT INTO Season(fallYear, userName, id, totalDistance) 
										VALUES(:fallYear, :userName, :id, :totalDistance)");
			else 
				$stmt = $dbnode->prepare("INSERT INTO Season(fallYear, userName, totalDistance) 
											VALUES(:fallYear, :userName, :totalDistance)");
			$stmt->bindValue(':fallYear', $this->fallYear);
			$stmt->bindValue(':userName', $this->userName);
			if ($club != null)
				$stmt->bindValue(':id', $this->id);
			$stmt->bindValue(':totalDistance', $this->totalDistance);
			$stmt->execute();
		} else {
			echo "dbnode failed&";
		}
	}
}

?>