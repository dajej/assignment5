<?php
include_once("clubs.php");
include_once("skier.php");
include_once("season.php");
include_once("db.php");
include_once("entry.php");


class XMLmodel{
	public $db = null;
	public $doc = null;
	public $DBfor = null;
	public $xpath = null;
	public function __construct(){
	$this->doc = new DOMdocument;
	$this->doc->load("SkierLogs.xml");
	$this->xpath = new DOMxpath($this->doc);
	$this->DBfor = new DBnode($this->db);
	$this->readclubs();
	$this->readskier();
	$this->readseason();
	$this->readentry();
	}
   public function readclubs(){
	   if($this->doc){
	   $ClubsNodes = $this->doc->getElementsByTagName("Club");
	   for($a = 0; $a < $ClubsNodes->length; $a++){
		$club = new Club($ClubsNodes[$a], $this->db);
		}
	   }
   }
   
   public function readskier(){
	   if($this->doc){
		   $SkierNodes = $this->xpath->query("//SkierLogs/Skiers/Skier");
		   for($a = 0; $a < $SkierNodes->length; $a++){
			$skier = new Skier($SkierNodes[$a], $this->db);
		   }
	   }
   }
   
	public function readSeason() {
		if($this->doc){ 
	    	$SeasonNodes = $this->doc->getElementsByTagName("Season");

	    	foreach ($SeasonNodes as $Season) {
		    	$year = $Season->getAttribute("fallYear");
		    	$Skiers = $Season->getElementsByTagName("Skiers");
		    	foreach ($Skiers as $Skis) {
		    		$club = null;
		    		if ($Skis->hasAttribute("clubId"))
		    			$club = $Skis->getAttribute("clubId");
		    		$Skier = $Skis->getElementsByTagName("Skier");
		    		foreach ($Skier as $Ski) {
		    			$distance = 0;
		    			$name = $Ski->getAttribute("userName");
		    			$EntryNodes = $Ski->getElementsByTagName("Entry");
		    			for ($a = 0; $a < $EntryNodes->length; $a++) {
		    				$distance += (int) $EntryNodes->item($a)->getElementsByTagName("Distance")->item(0)->nodeValue;
		    			}
		    			$season = new Season($distance, $year, $club, $name, $this->db);
		    		}
		    		
		    	}
		    }
	    }
    }
	public function readEntry() {
		if($this->doc){
	    	$SeasonNodes = $this->doc->getElementsByTagName("Season");

	    	foreach ($SeasonNodes as $Season) {
		    	$year = $Season->getAttribute("fallYear");
		    	$Skiers = $Season->getElementsByTagName("Skiers");
		    	foreach ($Skiers as $Skis) {
		    		$Skier = $Skis->getElementsByTagName("Skier");
		    		foreach ($Skier as $Ski) {
		    			$name = $Ski->getAttribute("userName");
		    			$EntryNodes = $Ski->getElementsByTagName("Entry");
		    			foreach ($EntryNodes as $Entry) {
		    				$entry = new Entry($Entry, $year, $name, $this->db);
		    			}
		    		}
		    	}
		    }
	    }
    }
}
?>

