<?php

class Entry {
	private $userName = null;
	private $fallYear = null;
	private $sdate = null;
	private $area = null;

	public function __construct($doc, $year, $username, $dbnode)  
    {
		if($dbnode){
        	$this->userName = $username;
        	$this->fallYear = $year;
	    	$this->sdate = $doc->getElementsByTagName("Date")->item(0)->nodeValue;
	    	$this->area = $doc->getElementsByTagName("Area")->item(0)->nodeValue;
    	    $this->distance = $doc->getElementsByTagName("distance")->item(0)->nodeValue;
	
			$stmt = $dbnode->prepare("INSERT INTO entry(userName, fallYear, sdate, area, distance) 
										VALUES(:userName, :fallYear, :sdate, :area, :distance)");
			$stmt->bindValue(':userName', $this->username);
			$stmt->bindValue(':fallYear', $this->fallYear);
			$stmt->bindValue(':sdate', $this->sdate);
			$stmt->bindValue(':area', $this->area);
			$stmt->bindValue(':distance', $this->distance);
			$stmt->execute();
		} else {
			echo "dbnode failed&";
		}
	}
}
?>