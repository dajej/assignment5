<?php

class Skier{
	private $userName = null;
	private $firstName = null;
	private $lastName = null;
	private $yearOfbirth= null;

	public function __construct($doc, $dbnode)  
    {
		if($dbnode){
        $this->userName = $doc->getAttribute("userName");
        $this->firstName = $doc->getElementsByTagName("FirstName")->item(0)->nodeValue;
	    $this->lastName = $doc->getElementsByTagName("LastName")->item(0)->nodeValue;
	    $this->yearOfbirth = $doc->getElementsByTagName("YearOfBirth")->item(0)->nodeValue;
    
		$stmt = $dbnode->prepare("INSERT INTO skier(userName, firstName, lastName, yearOfbirth) 
									VALUES(:userName, :firstName, :lastName, :yearOfbirth)");
		$stmt->bindValue(':userName', $this->userName);
		$stmt->bindValue(':firstName', $this->firstName);
		$stmt->bindValue(':lastName', $this->lastName);
		$stmt->bindValue(':yearOfbirth', $this->yearOfbirth);
		$stmt->execute();
}
else{
	echo "dbnode failed";
		}
	}
}
?>